#include "juego.h"

Juego::Juego(string plugins, string cfg, string recursos)
{
    MotorOgre motor(plugins, cfg);
    motor.cargarRecursos(recursos);

    raiz = motor.getRaiz();
    ventana = motor.getVentana();

}

Juego::~Juego()
{
}


void Juego::crearEscena()
{
    cout << "-- Creando escena..." << endl;

    // Escena con sombras y luz ambiental
    escena = raiz->createSceneManager(ST_GENERIC);
    escena->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
    escena->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
//    escena->setAmbientLight(Ogre::ColourValue(1,1,1));
//    escena->setSkyDome(true, "cielo", 5, 8, 500);

    // Luces

    Light* luz = escena->createLight("Luz2");
    luz->setType(Light::LT_POINT);
    luz->setPosition(Ogre::Vector3(-5,10,-5));

    // Camara

    camara = escena->createCamera("Camara");
    camara->setPosition(Ogre::Vector3(9,12,0));
    camara->lookAt(Ogre::Vector3(0,0,0));
    camara->setNearClipDistance(5);
    camara->setFarClipDistance(10000);

    // Viewport en toda la ventana
    Viewport* viewport = ventana->addViewport(camara);
    viewport->setBackgroundColour(ColourValue(0.0,0.0,0.0));

    double width = viewport->getActualWidth();
    double height = viewport->getActualHeight();
    camara->setAspectRatio(Real(width / height));

}

void Juego::iniciar()
{

    cout << "-- Iniciando Overlays..." << endl;
    this->overlays = OverlayManager::getSingletonPtr();
    cout << "-- Creando FrameListener..." << endl;
    this->frames = new MyFrameListener (ventana, camara, overlays, escena);
    raiz->addFrameListener(frames);

    cout << "-- Inicio del bucle infinito" << endl;
    this->raiz->startRendering();

}

void Juego::anadirLuz()
{
}

void Juego::anadirEscenario()
{
    // Geometria estatica
    cout << "-- Insertar escenaro" << endl;

    StaticGeometry* escenario = escena->createStaticGeometry("escenario");
    Entity* suelo = escena->createEntity("suelo.mesh");
    escenario->addEntity(suelo, Ogre::Vector3(0,0,0));
    escenario->build();

}

void Juego::anadirMenu()
{
    SceneNode* nodoMenu = escena->getRootSceneNode()->createChildSceneNode("nodoMenu");
    SceneNode* nodoJugar = nodoMenu->createChildSceneNode("nodoJugar");
    SceneNode* nodoLogros = nodoMenu->createChildSceneNode("nodoLogros");
    SceneNode* nodoAcerca = nodoMenu->createChildSceneNode("nodoAcerca");
    SceneNode* nodoSalir = nodoMenu->createChildSceneNode("nodoSalir");


    Entity* jugar = escena->createEntity("jugar.mesh");
    Entity* logros = escena->createEntity("logros.mesh");
    Entity* acerca = escena->createEntity("acerca.mesh");
    Entity* salir = escena->createEntity("salir.mesh");

    nodoJugar->attachObject(jugar);
    nodoLogros->attachObject(logros);
    nodoAcerca->attachObject(acerca);
    nodoSalir->attachObject(salir);

    jugar->setQueryFlags(MASCARA_JUGAR);
    logros->setQueryFlags(MASCARA_LOGROS);
    acerca->setQueryFlags(MASCARA_ACERCA);
    salir->setQueryFlags(MASCARA_SALIR);

}

void Juego::anadirTanques()
{
    SceneNode* nodoTanques = escena->getRootSceneNode()->createChildSceneNode("nodoTanques");
    SceneNode* nodoTanque1 = nodoTanques->createChildSceneNode("nodoTanque1");
    SceneNode* nodoTanque2 = nodoTanques->createChildSceneNode("nodoTanque2");
    SceneNode* nodoTanque3 = nodoTanques->createChildSceneNode("nodoTanque3");
    SceneNode* nodoTanque4 = nodoTanques->createChildSceneNode("nodoTanque4");
    SceneNode* nodoTanque5 = nodoTanques->createChildSceneNode("nodoTanque5");
    SceneNode* nodoTanque6 = nodoTanques->createChildSceneNode("nodoTanque6");
    SceneNode* nodoTanque7 = nodoTanques->createChildSceneNode("nodoTanque7");
    SceneNode* nodoTanque8 = nodoTanques->createChildSceneNode("nodoTanque8");
    SceneNode* nodoTanque9 = nodoTanques->createChildSceneNode("nodoTanque9");

    Entity* t1 = escena->createEntity("T1.mesh");
    Entity* t2 = escena->createEntity("T2.mesh");
    Entity* t3 = escena->createEntity("T3.mesh");
    Entity* t4 = escena->createEntity("T4.mesh");
    Entity* t5 = escena->createEntity("T5.mesh");
    Entity* t6 = escena->createEntity("T6.mesh");
    Entity* t7 = escena->createEntity("T7.mesh");
    Entity* t8 = escena->createEntity("T8.mesh");
    Entity* t9 = escena->createEntity("T9.mesh");

    nodoTanque1->attachObject(t1);
    nodoTanque2->attachObject(t2);
    nodoTanque3->attachObject(t3);
    nodoTanque4->attachObject(t4);
    nodoTanque5->attachObject(t5);
    nodoTanque6->attachObject(t6);
    nodoTanque7->attachObject(t7);
    nodoTanque8->attachObject(t8);
    nodoTanque9->attachObject(t9);

    t1->setQueryFlags(TANQUES);
    t2->setQueryFlags(TANQUES);
    t3->setQueryFlags(TANQUES);
    t4->setQueryFlags(TANQUES);
    t5->setQueryFlags(TANQUES);
    t6->setQueryFlags(TANQUES);
    t7->setQueryFlags(TANQUES);
    t8->setQueryFlags(TANQUES);
    t9->setQueryFlags(TANQUES);

    nodoTanques->setVisible(false);
}
