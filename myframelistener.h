#ifndef MYFRAMELISTENER_H
#define MYFRAMELISTENER_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <iostream>

#include "interaccion.h"
#include "tanque.h"
#include "musica.h"
#include "puntuacion.h"
#include "tiempo.h"

#define INTRO       0
#define MENU        1
#define JUGAR       2
#define GAMEOVER    3
#define LOGROS      4
#define ACERCA      5
#define SALIR       6

#define MASCARA_JUGAR   1 << 0
#define MASCARA_ACERCA  1 << 1
#define MASCARA_LOGROS  1 << 2
#define MASCARA_SALIR   1 << 3

#define TANQUES 1 << 4


using namespace std;
using namespace Ogre;

class MyFrameListener : public FrameListener
{
public:
    MyFrameListener(RenderWindow* ventana,
                    Camera* camara,
                    OverlayManager* overlays,
                    SceneManager* escena);

    ~MyFrameListener();
    bool frameStarted(const FrameEvent &evt);

    Ray setRayQuery(int posx, int posy, uint32 mask);
private:
    RenderWindow* ventana;
    Camera * camara;
    SceneNode* nodo;
    OverlayManager* overlays;
    SceneManager* escena;

    Interaccion* interaccion;

    int estadoActual;
    int estadoAnterior;
    bool continuar;

    RaySceneQuery* rayoEscena;
    uint32 mascara;
    SceneNode* nodoMenu;
    SceneNode* nodoTanques;

    OverlayElement* overlayCursor;
    OverlayElement* overlayIntro;
    OverlayElement* overlayAcerca;
    OverlayElement* overlayGameOver;
    OverlayElement* overlayTiempo;
    OverlayElement* overlayRecord;

    Ogre::Real tiempo;

    std::vector<Tanque*> tanques;

    Musica musica;

    Puntuacion* puntos;

    Tiempo* tiempoJuego;



    void intro(int &estadoActual, int &estadoAnterior);
    void menu(int &estadoActual, int &estadoAnterior);
    void jugar(int &estadoActual, int &estadoAnterior, const FrameEvent &evt);
    void gameOver();
    void logros(int &estadoActual, int &estadoAnterior);
    void acerca(int &estadoActual, int &estadoAnterior);
    void salir();

};

#endif // MYFRAMELISTENER_H
