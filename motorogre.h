#ifndef MOTOROGRE_H
#define MOTOROGRE_H

#include <Ogre.h>
#include <string>


using namespace std;
using namespace Ogre;

class MotorOgre
{
public:
    MotorOgre(string plugins, string cfg);
    ~MotorOgre();
    void cargarRecursos(string recursos);
    Root* getRaiz();
    RenderWindow* getVentana();

private:
    Root *raiz;
    RenderWindow * ventana;

};

#endif // MOTOROGRE_H
