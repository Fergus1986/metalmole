#include "juego.h"
#include "motorogre.h"
#include "musica.h"


#include "Ogre.h"
#include "SDL/SDL_mixer.h"

#include <iostream>
#include <string>

using namespace std;
using namespace Ogre;

int main()
{
    cout << "-- MetalMole 1.0" << endl;

    Juego juego("plugins.cfg", "ogre.cfg", "resources.cfg");

    juego.crearEscena();
    juego.anadirMenu();
    juego.anadirEscenario();
    juego.anadirTanques();



    juego.iniciar();

    return 0;
}

