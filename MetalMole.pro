TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


INCLUDEPATH += /usr/include/OIS/
INCLUDEPATH += /usr/include/SDL/
INCLUDEPATH += /usr/include/bullet/
INCLUDEPATH += /usr/include/OGRE/


CONFIG += link_pkgconfig
PKGCONFIG += OGRE OIS bullet sdl

LIBS += -lGL -lstdc++ -lOgreMain -lOIS -lSDL -lSDL_mixer

HEADERS += \
    juego.h

SOURCES += \
    juego.cpp

HEADERS += \
    motorogre.h

SOURCES += \
    motorogre.cpp

HEADERS += \
    musica.h

SOURCES += \
    musica.cpp

SOURCES += main.cpp

HEADERS += \
    myframelistener.h

SOURCES += \
    myframelistener.cpp

HEADERS += \
    interaccion.h

SOURCES += \
    interaccion.cpp

HEADERS += \
    tanque.h

SOURCES += \
    tanque.cpp

HEADERS += \
    puntuacion.h

SOURCES += \
    puntuacion.cpp

HEADERS += \
    tiempo.h

SOURCES += \
    tiempo.cpp
