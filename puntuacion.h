#ifndef PUNTUACION_H
#define PUNTUACION_H
#include <Ogre.h>

using namespace Ogre;

class Puntuacion
{
public:
    Puntuacion(RenderWindow *win,OverlayManager *overlays);
    int getPunt();
    void setPunt(int valor);
    void incrementarPunt(int valor);
    void decrementarPunt(int valor);
    void resetPunt();
    void MostrarPunt();
    void OcultarPunt();

private:
    int _punt;
    OverlayElement* overlayPunt,*Opuntos;
    RenderWindow * ventana;
    OverlayManager* overMan;
    Overlay* puntuacion;
};

#endif // PUNTUACION_H
