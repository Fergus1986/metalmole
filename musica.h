#ifndef MUSICA_H
#define MUSICA_H

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <string>

using namespace std;


class Musica
{
public:
    Musica();
    ~Musica();
    void cargarAudio(string audio);
    void cargarSonido(string sonido);
    void reproducirSonido();
    void reproducirAudio(double posicion);
    void pararAudio();
    void adelantarAudio(double posicion);

private:
    Mix_Music* id;
    Mix_Chunk *sonido;

};

#endif // MUSICA_H
