#include "interaccion.h"

Interaccion::Interaccion(RenderWindow *ventana)
{
    // Ventana de eventos
    size_t ventanaHardware = 0;
    ventana->getCustomAttribute("WINDOW", &ventanaHardware);
    this->entrada = InputManager::createInputSystem(ventanaHardware);

    // Manejador Teclado
    this->teclado = static_cast<Keyboard*>(entrada->createInputObject(OISKeyboard, true));
    this->teclado->setEventCallback(this);


    // Manejador de Raton
    this->raton = static_cast<Mouse*>(entrada->createInputObject(OISMouse, true));
    this->raton->getMouseState().width = ventana->getWidth();
    this->raton->getMouseState().height = ventana->getHeight();
    this->raton->setEventCallback(this);

    this->escape = false;

    // Intervalo entre clicks
    this->intervaloClick = new Timer();

}

Interaccion::~Interaccion()
{
}

bool Interaccion::getEscape()
{
    return this->escape;
}

int Interaccion::getRatonX()
{
    return this->raton->getMouseState().X.abs;
}

int Interaccion::getRatonY()
{
    return this->raton->getMouseState().Y.abs;
}


bool Interaccion::getClick()
{
    bool devolver= false;

    if (this->raton->getMouseState().buttonDown(OIS::MB_Left)){

        if(this->intervaloClick->getMilliseconds() > INTERVALO){
            devolver = true;
//            cout << "click: " << intervaloClick->getMilliseconds() << endl;
            this->intervaloClick->reset();
        }

    }

    return devolver;
}

void Interaccion::capturarTeclado()
{
    this->teclado->capture();
}

void Interaccion::capturarRaton()
{
    this->raton->capture();
}

void Interaccion::setEscape(bool escape)
{
    this->escape=escape;
}


bool Interaccion::keyPressed(const KeyEvent &arg)
{
    if (arg.key == OIS::KC_ESCAPE){
        cout << "Escape presionado " << endl;
        this->escape = true;
    }

    return true;
}

bool Interaccion::keyReleased(const KeyEvent &arg)
{
}

bool Interaccion::mouseMoved(const MouseEvent &arg)
{
}

bool Interaccion::mousePressed(const MouseEvent &arg, MouseButtonID id)
{
}

bool Interaccion::mouseReleased(const MouseEvent &arg, MouseButtonID id)
{
}
