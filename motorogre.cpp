#include "motorogre.h"



MotorOgre::MotorOgre(string plugins, string cfg)
{
    cout << "-- Inicializacion de Ogre" << endl;
    this->raiz = new Root(plugins, cfg);

    // Ventana de configuracion

    // Elegir tipo de render

//    this->raiz->showConfigDialog();

    if(!raiz->restoreConfig()){
        raiz->showConfigDialog();
        raiz->saveConfig();
    }

    this->ventana = raiz->initialise(true, "MetalMole");


}

MotorOgre::~MotorOgre()
{
}

void MotorOgre::cargarRecursos(string recursos)
{
    Ogre::ConfigFile configuracion;
    configuracion.load(recursos);

    cout << "-- Inicio de carga de recursos..." << endl;
    Ogre::ConfigFile::SectionIterator sI = configuracion.getSectionIterator();
    Ogre::String seccion, tipo, datos;

    while(sI.hasMoreElements()){
        seccion = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap * sM = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;

        for (i=sM->begin(); i!=sM->end(); ++i){
            tipo = i->first;
            datos = i->second;
            Ogre::ResourceGroupManager::getSingleton()
                    .addResourceLocation(datos, tipo, seccion);
        }
    }

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    cout << "-- Recursos cargados" << endl;


}

Ogre::Root *MotorOgre::getRaiz()
{
    return this->raiz;
}

Ogre::RenderWindow *MotorOgre::getVentana()
{
    return this->ventana;
}
