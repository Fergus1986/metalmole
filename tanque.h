#ifndef TANQUE_H
#define TANQUE_H

#include <Ogre.h>
#include <string>


using namespace Ogre;
using namespace std;

class Tanque
{
public:
    Tanque(SceneNode *nodo, string nombre, Real tiempoVida);
    ~Tanque();
    void cambiarVisibilidad();
    void disparar();
    string getNombre();
    Real getTiempoVida();
    void incrementarContador(Real incremento);
    bool tiempoAgotado();
    void reiniciarContador();
    Real getContador();
    bool getVisibilidad();

private:
    string nombre;
    SceneNode* nodo;
    Real tiempoVida;
    Real contador;
    bool visibilidad;
};

#endif // TANQUE_H
