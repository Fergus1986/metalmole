#ifndef TIEMPO_H
#define TIEMPO_H
#include <Ogre.h>
using namespace Ogre;
using namespace std;

#define MAX_TIEMPO  65

class Tiempo
{
public:
    Tiempo();
    ~Tiempo();
    void pausa();
    void reset();
    float getMilisegundos();
    int getContador();
    void aumentarContador(int n);
    void disminuirContador(int n);
    bool tiempoAgotado();
    void reiniciar();
private:
    Ogre::Timer* crono;
    int cont;
};

#endif // TIEMPO_H
