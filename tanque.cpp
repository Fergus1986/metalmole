#include "tanque.h"

Tanque::Tanque(SceneNode *nodo, string nombre, Real tiempoVida)
{
    this->nombre = nombre;
    this->nodo=static_cast<SceneNode*>(nodo->getChild(nombre));
    this->tiempoVida=tiempoVida;
    this->contador=0;
    this->visibilidad=false;
}


void Tanque::cambiarVisibilidad()
{
    this->nodo->flipVisibility();

    if(this->visibilidad== false){
        this->visibilidad=true;
    }else{
        this->visibilidad=false;
    }

}

void Tanque::disparar()
{
    this->nodo->setVisible(false);
    this->reiniciarContador();
    this->visibilidad=false;
}


string Tanque::getNombre()
{
    return this->nombre;
}

Real Tanque::getTiempoVida()
{
    return this->tiempoVida;
}

void Tanque::incrementarContador(Real incremento)
{
    this->contador += incremento;
}

bool Tanque::tiempoAgotado()
{
    bool devolver=false;
    if(this->contador >= this->tiempoVida){
        devolver = true;
    }

    return devolver;
}

void Tanque::reiniciarContador()
{
    this->contador = 0;
}

Real Tanque::getContador()
{
    return this->contador;
}

bool Tanque::getVisibilidad()
{
    return this->visibilidad;
}
