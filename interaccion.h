#ifndef INTERACCION_H
#define INTERACCION_H

#include <OIS/OIS.h>
#include <Ogre.h>

#include <vector>

#define INTERVALO 333

using namespace std;
using namespace Ogre;
using namespace OIS;

class Interaccion : public OIS::KeyListener, public OIS::MouseListener
{
public:
    Interaccion(RenderWindow* ventana);
    ~Interaccion();
    bool getEscape();
    int getRatonX();
    int getRatonY();
    bool getClick();
    void capturarTeclado();
    void capturarRaton();

    void setEscape(bool escape);



    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg);
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);


private:
    InputManager* entrada;
    Keyboard* teclado;
    Mouse* raton;

    bool escape;
    int ratonX;
    int ratonY;
    bool click;
    Timer* intervaloClick;
};

#endif // INTERACCION_H
