#include "musica.h"

#include <iostream>


Musica::Musica()
{
    cout << "-- Iniciando sistemas de Audio..." << endl;
    // Iniciamos el subsistema SDL_Audio
    if(SDL_Init(SDL_INIT_AUDIO) < 0){
        cerr << "Error al iniciar SDL_Audio" << SDL_GetError() << endl;
        exit(1);
    }

    // Iniciamos la biblioteca SDL_Mixer
    if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
                     MIX_DEFAULT_CHANNELS, 4096) < 0){
        cerr << "Error al iniciar SDL_Mixer" << endl;
    }

}

Musica::~Musica()
{
}

void Musica::cargarAudio(string audio)
{
    this->id = Mix_LoadMUS(audio.c_str());

    if (id == NULL){
        cerr << "Error al cargar el archivo";
        cerr << Mix_GetError() << endl;
    }
}

void Musica::cargarSonido(string sonido)
{
    this->sonido = Mix_LoadWAV(sonido.c_str());

    if(this->sonido == NULL){
        cerr << "Error al cargar el archivo";
        cerr << Mix_GetError() << endl;
    }

    Mix_VolumeChunk(this->sonido, 15);
    Mix_AllocateChannels(2);
}

void Musica::reproducirSonido()
{
    Mix_PlayChannel(1, this->sonido, 0);
}

void Musica::reproducirAudio(double posicion)
{
    // id, repeticiones, fade in (ms), posicion
    if (Mix_FadeInMusicPos(this->id, 0, 1000, posicion) == -1){
        cerr << "Error al reproducir archivo";
        cerr << Mix_GetError() << endl;

    }

    Mix_VolumeMusic(MIX_MAX_VOLUME);
}

void Musica::adelantarAudio(double posicion)
{
    Mix_RewindMusic();
    if(Mix_SetMusicPosition(posicion)==-1) {
        cerr << "Error al avanzar el archivo";
        cerr << Mix_GetError() << endl;
    }

}

void Musica::pararAudio()
{
    Mix_PauseMusic();

}
