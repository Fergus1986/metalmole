#include "puntuacion.h"

Puntuacion::Puntuacion(RenderWindow *win,OverlayManager *overlays)
{
    this->_punt=0;
    this->overMan=overlays;
    this->ventana=win;
    Overlay* puntuacion = this->overMan->getByName("Puntuacion");
    puntuacion->show();
    this->overlayPunt= this->overMan->getOverlayElement("puntuacion");
    this->overlayPunt->hide();

//    this->overlayPunt->setHeight(this->ventana->getHeight()/5);
//    this->overlayPunt->setWidth(this->ventana->getWidth()/5);

    this->Opuntos=overMan->getOverlayElement("punt");


}
int Puntuacion::getPunt(){
    return this->_punt;
}
void Puntuacion::setPunt(int valor){
    this->_punt=valor;
}

void Puntuacion::incrementarPunt(int valor){
    this->_punt+=valor;
}

void Puntuacion::decrementarPunt(int valor){
    this->_punt-=valor;
}

void Puntuacion::resetPunt(){
    this->_punt=0;
}
void Puntuacion::MostrarPunt(){
    //this->puntuacion->show();
    this->Opuntos->setCaption(Ogre::StringConverter::toString(this->getPunt()));
    this->overlayPunt->show();
}
void Puntuacion::OcultarPunt(){
    this->overlayPunt->hide();
}
