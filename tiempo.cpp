#include "tiempo.h"

Tiempo::Tiempo()
{
    this->crono = new Timer();
    this->cont = MAX_TIEMPO;
}

Tiempo::~Tiempo(){

}

void Tiempo::reset(){
    crono->reset();
}

void Tiempo::pausa(){

}

float Tiempo::getMilisegundos(){
    return this->crono->getMilliseconds();;
}

int Tiempo::getContador(){
    return cont;
}

void Tiempo::aumentarContador(int n){
    cont = cont + n;
}

void Tiempo::disminuirContador(int n){
    cont = cont - n;
}

bool Tiempo::tiempoAgotado(){
    if(this->cont==0)
        return true;
    else
        return false;
}

void Tiempo::reiniciar()
{
    this->cont = MAX_TIEMPO;
}
