#ifndef JUEGO_H
#define JUEGO_H

#include <Ogre.h>
#include <SDL/SDL_mixer.h>
#include <string>

#include "motorogre.h"
#include "musica.h"
#include "myframelistener.h"

using namespace Ogre;

class Juego
{
public:
    Juego(string plugins, string cfg, string recursos);
    ~Juego();
    void crearEscena();
    void iniciar();
    void anadirLuz();
    void anadirEscenario();
    void anadirMenu();
    void anadirTanques();

private:
    Root* raiz;
    Camera* camara;
    RenderWindow* ventana;
    SceneManager *escena;
    FrameListener* frames;
    OverlayManager* overlays;

};

#endif // JUEGO_H
