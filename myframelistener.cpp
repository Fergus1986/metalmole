#include "myframelistener.h"


MyFrameListener::MyFrameListener(RenderWindow *ventana, Camera *camara, OverlayManager *overlays, SceneManager *escena)
{
    this->ventana = ventana;
    this->camara=camara;
    this->overlays=overlays;
    this->escena=escena;

    this->estadoActual = INTRO;
    this->estadoAnterior = SALIR;
    this->continuar = true;

    // Interaccion de raton y teclado
    this->interaccion = new Interaccion(ventana);

    // Overlays
    Overlay* cursor = this->overlays->getByName("Cursor");
    Overlay* intro = this->overlays->getByName("Intro");
    Overlay* acerca = this->overlays->getByName("Acerca");
    Overlay* gameOver = this->overlays->getByName("Fin");
    Overlay* record = this->overlays->getByName("Record");
    cursor->show();
    intro->show();
    acerca->show();
    gameOver->show();
    record->show();

    this->overlayCursor = this->overlays->getOverlayElement("cursor");
    this->overlayIntro = this->overlays->getOverlayElement("intro");
    this->overlayIntro->setHeight(this->ventana->getHeight());
    this->overlayIntro->setWidth(this->ventana->getWidth());
//    this->overlayCursor->setWidth(this->ventana->getWidth() / 9);
//    this->overlayCursor->setHeight(this->ventana->getHeight() / 9);

    this->overlayAcerca=this->overlays->getOverlayElement("acerca");
    this->overlayAcerca->setHeight(this->ventana->getHeight());
    this->overlayAcerca->setWidth(this->ventana->getWidth());
    this->overlayGameOver=this->overlays->getOverlayElement("fin");
    this->overlayGameOver->setHeight(this->ventana->getHeight());
    this->overlayGameOver->setWidth(this->ventana->getWidth());

    this->overlayTiempo = this->overlays->getOverlayElement("time");
    this->overlayTiempo->hide();
    this->overlayGameOver->hide();

    this->overlayRecord=this->overlays->getOverlayElement("record");
    this->overlayRecord->setHeight(this->ventana->getHeight());
    this->overlayRecord->setWidth(this->ventana->getWidth());
    this->overlayRecord->hide();

    // Tiempo
    this->tiempo = 0;

    // Interactuar con objetos
    this->mascara=0;
    this->rayoEscena = this->escena->createRayQuery(Ray());

    // Interactuar con el menu
    this->nodoMenu = static_cast<SceneNode*>
            (this->escena->getRootSceneNode()->getChild("nodoMenu"));

    // Interactuar con tanques
    this->nodoTanques = static_cast<SceneNode*>
            (this->escena->getRootSceneNode()->getChild("nodoTanques"));

    // Semilla aleatoria
    srand(unsigned(time(NULL)));

    // Tanques
    int auxiliar;
    Real t;
    for(int i=1; i<10; i++){
        stringstream nombre;
        nombre << "nodoTanque" << i;
        t = ((rand() % 4000) + 500)/1000.0;
        cout << "n: " << nombre.str() << "\tt: " << t << endl;
        this->tanques.push_back(new Tanque(this->nodoTanques, nombre.str(), t));
        nombre.operator >>(auxiliar);
    }

    // Puntuacion
    this->puntos = new Puntuacion(this->ventana, this->overlays);

    // Tiempo
    this->tiempoJuego = new Tiempo();

    // Musica
    this->musica.cargarAudio("audio/Peer Gynt Suite No. 1, Op. 46 - IV. In the Hall Of The Mountain King.mp3");
    this->musica.cargarSonido("audio/Bomb 2-SoundBible.com-953367492.wav");


}

MyFrameListener::~MyFrameListener()
{
}

bool MyFrameListener::frameStarted(const FrameEvent &evt)
{

    // Capturar eventos de raton y teclado
    this->interaccion->capturarRaton();
    this->interaccion->capturarTeclado();

    // Overlay del cursor
    this->overlayCursor->setLeft(this->interaccion->getRatonX() - 132);
    this->overlayCursor->setTop(this->interaccion->getRatonY() - 77);
    this->overlayCursor->show();

    switch(estadoActual){

    case INTRO:
        intro(estadoActual, estadoAnterior);
        break;
    case MENU:
        menu(estadoActual, estadoAnterior);
        break;
    case JUGAR:
        jugar(estadoActual, estadoAnterior, evt);
        break;
    case GAMEOVER:
        gameOver();
        break;
    case LOGROS:
        logros(estadoActual, estadoAnterior);
        break;
    case ACERCA:
        acerca(estadoActual, estadoAnterior);
        break;
    case SALIR:
        salir();
        break;
    }

    return continuar;
}

void MyFrameListener::intro(int &estadoActual, int &estadoAnterior)
{
//    cout << "Intro()" << endl;

    // Dibujar Intro
    if(estadoActual != estadoAnterior){
        this->overlayIntro->show();
        estadoAnterior = INTRO;
        this->overlayAcerca->hide();
//        this->overlayTiempo->hide();
    }

    // Interaccion raton
    if(this->interaccion->getClick()){
        estadoAnterior= INTRO;
        estadoActual = MENU;
    }

    // Interaccion teclado
    if(this->interaccion->getEscape()){
        estadoAnterior = INTRO;
        estadoActual = SALIR;
        this->interaccion->setEscape(false);
    }

    // Salir del estado
    if(estadoActual!= INTRO){
        // LIMPIAR PANTALLA
        this->overlayIntro->hide();
    }

}

void MyFrameListener::menu(int &estadoActual, int &estadoAnterior)
{
//    cout << "Menu()" << endl;

    // Dibujar Menu
    if(estadoAnterior != estadoActual){
        this->nodoMenu->setVisible(true);
        estadoAnterior = MENU;
//        this->overlayTiempo->hide();
    }

    // Interaccion del raton
    if(this->interaccion->getClick()){

        this->mascara = MASCARA_JUGAR | MASCARA_ACERCA | MASCARA_LOGROS | MASCARA_SALIR;
        setRayQuery(this->interaccion->getRatonX(), this->interaccion->getRatonY(), mascara);
        RaySceneQueryResult &resultado = this->rayoEscena->execute();
        RaySceneQueryResult::iterator iterador;
        iterador = resultado.begin();

        if(iterador != resultado.end()){

            //            cout << "nodo:" << iterador->movable->getParentSceneNode()->getName() << endl;

            if(iterador->movable->getParentSceneNode()->getName() == "nodoJugar"){
                estadoActual = JUGAR;

            }
            if(iterador->movable->getParentSceneNode()->getName() == "nodoSalir"){
                estadoActual = SALIR;

            }
            if(iterador->movable->getParentSceneNode()->getName() == "nodoLogros"){
                estadoActual = LOGROS;

            }
            if(iterador->movable->getParentSceneNode()->getName() == "nodoAcerca"){
                estadoActual = ACERCA;
            }
        }

    }

    // Interaccion del teclado
    if(this->interaccion->getEscape()){
        estadoAnterior = MENU;
        estadoActual = INTRO;
        this->interaccion->setEscape(false);
    }



    // Cambio de estado
    if(estadoActual != MENU){
        // LIMPIAR PANTALLA
        this->nodoMenu->setVisible(false);
    }
}

void MyFrameListener::jugar(int &estadoActual, int &estadoAnterior, const FrameEvent &evt)
{
    //    cout << "Jugar()" << endl;

    // Dibujar Jugar

    if(estadoActual != estadoAnterior){
        cout << "entrada limpia" << endl;
        this->nodoTanques->setVisible(false);
        estadoAnterior = JUGAR;
        this->tiempo = 0;
        this->tiempoJuego->reset();

        // Musica
        musica.reproducirAudio(74);

    }

    // Tiempo

    if(this->tiempoJuego->getMilisegundos() >= 1000.0){
        this->tiempoJuego->disminuirContador(1);
        this->tiempoJuego->reset();
    }

    // Tiempo
    Overlay* tiempoO = this->overlays->getByName("Tiempo");
    tiempoO->show();
    this->overlayTiempo->show();

    this->overlayTiempo->setCaption(Ogre::StringConverter::toString(this->tiempoJuego->getContador()));

    // Puntuacion
    this->puntos->MostrarPunt();

    // Incrementar tiempo
    tiempo = evt.timeSinceLastFrame;
    for(int unsigned i=0; i<tanques.size(); i++){
        tanques[i]->incrementarContador(tiempo);

        // Comprobar tiempo Agotado (max tiempo visible)
        if(tanques[i]->tiempoAgotado()){
            tanques[i]->cambiarVisibilidad();
            tanques[i]->reiniciarContador();
        }
    }

    // Interaccion del raton
    if(this->interaccion->getClick()){
        this->mascara = TANQUES;
        setRayQuery(this->interaccion->getRatonX(), this->interaccion->getRatonY(), mascara);
        RaySceneQueryResult &resultado = this->rayoEscena->execute();
        RaySceneQueryResult::iterator iterador;
        iterador = resultado.begin();


        if(iterador != resultado.end()){
            for(int unsigned i=0; i<this->tanques.size(); i++){
                string nombre = iterador->movable->getParentSceneNode()->getName();
                if(nombre == tanques[i]->getNombre()){

                    if(tanques[i]->getVisibilidad()){
                        this->puntos->incrementarPunt(100);
                        this->musica.reproducirSonido();
                        tanques[i]->disparar();
                    }
                }
            }
        }
    }


    // Interaccion del teclado
    if(this->interaccion->getEscape()){
        estadoAnterior = JUGAR;
        estadoActual = GAMEOVER;
        this->interaccion->setEscape(false);
        OverlayElement* texto = this->overlays->getOverlayElement("ptx");
        texto->setCaption(Ogre::StringConverter::toString(this->puntos->getPunt()));
    }

    // Condicion de fin de juego
    if(this->tiempoJuego->tiempoAgotado()){
        estadoActual = GAMEOVER;
        OverlayElement* texto = this->overlays->getOverlayElement("ptx");
        texto->setCaption(Ogre::StringConverter::toString(this->puntos->getPunt()));
    }



    // Cambio de estado
    if(estadoActual != JUGAR){
        // LIMPIAR PANTALLA
        this->nodoTanques->setVisible(false);
        this->musica.pararAudio();
        this->puntos->OcultarPunt();
        this->puntos->resetPunt();
        this->overlayTiempo->hide();
        tiempoO->hide();
        this->tiempoJuego->reiniciar();

    }
}

void MyFrameListener::gameOver()
{
//    cout << "GameOver()" << endl;

    // Dibujar GameOver
    this->overlayGameOver->show();
    this->overlayTiempo->hide();

    // Interaccion del teclado y raton
    if(this->interaccion->getEscape() || this->interaccion->getClick()){
        estadoAnterior = GAMEOVER;
        estadoActual = LOGROS;
        this->interaccion->setEscape(false);
    }


    if(estadoActual!= GAMEOVER){
        // LIMPIAR PANTALLA
        this->overlayGameOver->hide();

    }

}



void MyFrameListener::logros(int &estadoActual, int &estadoAnterior)
{
//    cout << "Logros()" << endl;

    // Dibujar Logros

    this->overlayRecord->show();
    this->overlayTiempo->hide();

    // Interaccion del teclado
    if(this->interaccion->getEscape() || this->interaccion->getClick()){
        estadoAnterior = LOGROS;
        estadoActual = MENU;
        this->interaccion->setEscape(false);
    }



    // Cambio de estado
    if(estadoActual!= LOGROS){
        // LIMPIAR PANTALLA
        this->overlayRecord->hide();

    }
}

void MyFrameListener::acerca(int &estadoActual, int &estadoAnterior)
{
//    cout << "Acerca()" << endl;

    // Dibujar Acerca

    this->overlayTiempo->hide();

    if(estadoAnterior != estadoActual){
        this->overlayAcerca->show();
    }


    // Interaccion del teclado
    if(this->interaccion->getEscape() || this->interaccion->getClick()){
        estadoAnterior = ACERCA;
        estadoActual = MENU;
        this->interaccion->setEscape(false);
    }



    // Cambio de estado
    if(estadoActual!= ACERCA){
        // LIMPIAR PANTALLA
        this->overlayAcerca->hide();

    }
}

void MyFrameListener::salir()
{
//    cout << "Salir()" << endl;

    // Salir

    this->continuar = false;
}

Ray MyFrameListener::setRayQuery(int posx, int posy, uint32 mask) {
    Ray rayMouse = this->camara->getCameraToViewportRay
            (posx/float(this->ventana->getWidth()), posy/float(this->ventana->getHeight()));
    this->rayoEscena->setRay(rayMouse);
    this->rayoEscena->setSortByDistance(true);
    this->rayoEscena->setQueryMask(mask);
    return (rayMouse);
}
